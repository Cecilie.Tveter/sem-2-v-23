package no.uib.inf101.sem2.View;

import java.awt.*;

public interface ColorTheme {

    Color getCellColor (char c);

    Color getFrameColor ();

    Color getBackgroundColor ();
}
