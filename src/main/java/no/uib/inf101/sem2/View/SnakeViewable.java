package no.uib.inf101.sem2.View;

import no.uib.inf101.sem2.Grid.GridCell;
import no.uib.inf101.sem2.Grid.GridDimension;
import no.uib.inf101.sem2.Model.SnakeBoard;

public interface SnakeViewable {

    GridDimension getDimension();

    Iterable<GridCell<Character>> getTilesOnBoard();

    SnakeBoard getSnakeBoard();


}
