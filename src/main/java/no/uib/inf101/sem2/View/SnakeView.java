package no.uib.inf101.sem2.View;

import no.uib.inf101.sem2.Grid.CellPosition;
import no.uib.inf101.sem2.Grid.CellPositionToPixelConverter;
import no.uib.inf101.sem2.Grid.GridCell;
import no.uib.inf101.sem2.Model.Apple;
import no.uib.inf101.sem2.Model.GameState;
import no.uib.inf101.sem2.Model.Snake;
import no.uib.inf101.sem2.Model.SnakeBoard;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class SnakeView extends JPanel {

    private final SnakeViewable snakeViewable;
    private final ColorTheme colorTheme;
    private final Snake snake;
    private Apple apple;
    private SnakeBoard board;

    public SnakeView (SnakeViewable snakeViewable, Snake snake, SnakeBoard board) {
        this.snake = snake;
        this.board = board;
        this.apple = new Apple(this.board);
        this.snakeViewable = snakeViewable;
        this.colorTheme = new DefaultColorTheme();
        this.setBackground(colorTheme.getBackgroundColor());
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(500, 500));
}

    public static void drawCells(Graphics2D g, Iterable<GridCell<Character>> iterable,
                                 CellPositionToPixelConverter pixelConverter,  ColorTheme colorTheme) {
        for (GridCell<Character> cell : iterable) {
            Rectangle2D rect = pixelConverter.getBoundsForCell(cell.pos());
            g.setColor(colorTheme.getCellColor(cell.value()));
            g.fill(rect);
        }
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(Color.black);
        g2.fillRect(0, 0, getWidth(), getHeight());

        if (snake.getGameState() == GameState.GAME_OVER){
            gameOver(g);
        }
        else {
            drawGame(g2);
            drawSnake(g2);
            drawApple(g2);



            // Må omstrukturere en god del av logikken i Model-klassen, slik at jeg kan kunne hente opp eple
            // hver gang en slange har spist eplet. Dette må gjøres!!

    }
    }


    public void drawSnake(Graphics g) {
        snake.move();
        for (Point point : snake.getBody()) {
            g.setColor(Color.GREEN);
            int cellSize = 25;
            g.fillRect(point.x * cellSize, point.y * cellSize, cellSize, cellSize);

        }
    }


    public void drawApple (Graphics g) {
        //apple.newApple();
        apple.newApple();
        g.setColor(Color.RED);
        g.fillOval(apple.appPos.row(), apple.appPos.col(), snakeViewable.getDimension().rows(), snakeViewable.getDimension().cols());
    }


    public void drawGame(Graphics2D g) {
        double margin = 2;
        double x = margin;
        double y = margin;
        double width = this.getWidth() - 2 * margin;
        double height = this.getHeight() - 2 * margin;

        Rectangle2D rect = new Rectangle2D.Double(x, y, width, height);

        g.setColor(colorTheme.getFrameColor());
        g.fill(rect);

        CellPositionToPixelConverter cellPosition = new CellPositionToPixelConverter(rect, snakeViewable.getDimension(), margin);

        drawCells(g, this.snakeViewable.getTilesOnBoard(), cellPosition, this.colorTheme);
        //drawApple(g);
        //drawSnake(g);

    }

    public void gameOver (Graphics g) {
        // Sette opp egen score for antall spiste epler

        // Game over text
        g.setColor(Color.red);
        g.setFont(new Font ("Ink Free", Font.BOLD, 75));
        FontMetrics metrics = getFontMetrics(g.getFont());
        int y = (getHeight() - metrics.getHeight()) / 2 + metrics.getAscent();
        int x = (getWidth() - metrics.stringWidth("Game Over")) / 2;

        g.drawString("Game Over",x, y);

    }

    }



