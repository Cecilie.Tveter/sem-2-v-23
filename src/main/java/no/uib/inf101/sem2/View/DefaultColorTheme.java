package no.uib.inf101.sem2.View;

import java.awt.*;

public class DefaultColorTheme implements ColorTheme{
    @Override
    public Color getCellColor(char c) {
        Color color = switch (c) {
            case 'r' -> Color.black;
            case 'g' -> Color.black;
            case '-' -> Color.black;
            default -> throw new IllegalArgumentException
            ("No available color for " + c + "'");
        };
        return color;
    }

    @Override
    public Color getFrameColor() {
        return Color.black;
    }

    @Override
    public Color getBackgroundColor() {
        return Color.black;
    }
}
