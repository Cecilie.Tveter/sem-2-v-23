package no.uib.inf101.sem2.Grid;

public record GridCell <E> (CellPosition pos, E value) {
}
