package no.uib.inf101.sem2.Grid;

public record CellPosition (int row, int col) {
}
