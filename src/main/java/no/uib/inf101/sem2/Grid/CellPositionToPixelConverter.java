package no.uib.inf101.sem2.Grid;

import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {
    Rectangle2D box;
    GridDimension gd;
    double margin;


    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        this.box = box;
        this.gd = gd;
        this.margin = margin;

    }
    /**
     * Denne metoden regner ut piksel-koordinatene til en rute basert på
     * dens posisjon (radnummer og kolonnenummer).
     *
     * @param cellPosition
     * @return
     */
    public Rectangle2D getBoundsForCell(CellPosition cellPosition) {
        double cellWidth = (box.getWidth() - margin * (gd.cols() + 1)) / gd.cols();
        double cellHeight = (box.getHeight() - margin * (gd.rows() + 1)) / gd.rows();
        double cellX = box.getX() + margin + cellPosition.col() * (margin + cellWidth);
        double cellY = box.getY() + margin + cellPosition.row() * (margin + cellHeight);
        return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    }
}
