package no.uib.inf101.sem2.Grid;

public interface GridDimension {

    int rows();

    int cols();
}
