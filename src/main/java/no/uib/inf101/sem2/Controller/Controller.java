package no.uib.inf101.sem2.Controller;

import no.uib.inf101.sem2.Model.*;
import no.uib.inf101.sem2.View.SnakeView;
import no.uib.inf101.sem2.View.SnakeViewable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controller implements KeyListener, ActionListener {
    static final int DELAY = 300;
    SnakeView snakeView;
    Snake snake;
    public Timer timer;
    Apple apple;
    boolean isAppleEaten = false;


    public Controller(SnakeView snakeView, Snake snake) {
        this.snake = snake;
        this.snakeView = snakeView;
        snakeView.addKeyListener(this);
        snakeView.setFocusable(true);
        snakeView.requestFocus();
        startGame();
    }

    public void startGame() {
        timer = new Timer(DELAY, this);
        timer.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        snake.move();
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if (snake.getDirection() != Direction.RIGHT) {
                    snake.setDirection(Direction.LEFT);
                    break;
                }
            case KeyEvent.VK_RIGHT:
                if (snake.getDirection() != Direction.LEFT) {
                    snake.setDirection(Direction.RIGHT);
                    break;
                }
            case KeyEvent.VK_UP:
                if (snake.getDirection() != Direction.DOWN) {
                    snake.setDirection(Direction.UP);
                    break;
                }
            case KeyEvent.VK_DOWN:
                if (snake.getDirection() != Direction.UP) {
                    snake.setDirection(Direction.DOWN);
                    break;
                }
        }
        snakeView.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(getGameScreen() == GameState.GAME_OVER) {
            snakeView.repaint();
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (getGameScreen() == GameState.ACTIVE_GAME) {
            if (snake.checkWallCollision()) {
                timer.stop();
            }
            if (snake.checkFoodCollision()) {
                    isAppleEaten = false;

            }
        }
            //snake.checkSelfCollision();
            snakeView.repaint();
        }

    public GameState getGameScreen() {
        return GameState.ACTIVE_GAME;
}

}


