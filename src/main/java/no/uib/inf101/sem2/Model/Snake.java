package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.View.SnakeViewable;

import java.awt.*;
import java.util.ArrayList;

public class Snake {
    private ArrayList<Point> body;
    private Direction direction;
    private SnakeViewable snakeViewable;
    private Apple apple;
    private SnakeModel model;
    private SnakeBoard board;
    GameState gameScreen = GameState.ACTIVE_GAME;
    private int appleEaten;
    private boolean isAppleEaten = true;

    public Snake(SnakeBoard board) {
        this.apple = new Apple(board);
        this.direction = Direction.RIGHT;
        this.board = board;
        this.body = new ArrayList<>();
        body.add(new Point(0, 0)); // Setter lokasjonen til snaken i punktet 0, 0.
        body.add(new Point(1, 0));
        body.add(new Point(2, 0));
    }
    /**
     * Gets the body of the snake
     *
     * @return Returns the body of the snake
     */
    public ArrayList<Point> getBody() {
        return body;
    }

    /**
     * This method changes the direction of the snake depending
     * on which direction the snake goes.
     */
    public void move() {
        Point head = body.get(0);

        for (int i = body.size() - 1; i > 0; i--) {
            Point segment = body.get(i);
            Point prevSegmnet = body.get(i - 1);
            segment.x = prevSegmnet.x;
            segment.y = prevSegmnet.y;
        }

        switch (direction) {
            case UP:
                head.y -= 1;
                break;
            case DOWN:
                head.y += 1;
                break;
            case LEFT:
                head.x -= 1;
                break;
            case RIGHT:
                head.x += 1;
                break;
            default:
                throw new IllegalArgumentException();
        }
    }


    /**
     * When a snake eats an apple the body of the snake will grow with
     * one part.
     */
    public void grow () {
        // Gets the position of the last element in the body
        Point lastPart = body.get(body.size() - 1);
        // Adds a new part to the body
        body.add(new Point(lastPart.x, lastPart.y));

    }

    /**
     * This method checks if the snake collides with the
     * boarders of the game.
     * @return Returns true if the snake dosent collide.
     * Returns false if the snake collides with the boardes.
     */
    public boolean checkWallCollision() {
        Point head = getBody().get(0);
        int x = head.x;
        int y = head.y;

        // Check if the head has hit any of the walls
        if (x < 0 || x >= this.board.rows()
                || y < 0 || y >= this.board.cols()) {
            this.gameScreen = GameState.GAME_OVER;
            return true;
        }
        return false;
    }
    /**
     *
     * @return
     */
    public boolean checkFoodCollision() {
        Point head = getBody().get(0);
            if (((head.x == apple.getAppPos().row())) && head.y == apple.getAppPos().col()) {
                grow();
                appleEaten++;
                apple.newApple();
                //isAppleEaten = false;
                return true;
            }
            return false;
        }


    /**
     *
     * @return
     */
    public boolean checkSelfCollision() {
        Point head = getBody().get(0);
        for (int i = 0; i < getBody().size(); i--) {
            if (head.equals(getBody().get(i))) {
                this.gameScreen = GameState.GAME_OVER;
                return true;
            }
        }
        return false;
    }


    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public GameState getGameState () {
        return gameScreen;
    }
}



