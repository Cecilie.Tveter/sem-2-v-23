package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.Grid.Grid;
import no.uib.inf101.sem2.Grid.GridCell;
import no.uib.inf101.sem2.Grid.GridDimension;
import no.uib.inf101.sem2.View.SnakeViewable;

public class SnakeModel implements SnakeViewable {
    SnakeBoard snakeBoard;

    public SnakeModel (SnakeBoard snakeBoard) {
        this.snakeBoard = snakeBoard;
    }


    @Override
    public GridDimension getDimension() {
        return new Grid<>(snakeBoard.rows(), snakeBoard.cols());
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return snakeBoard;
    }


    /**
     * Gets the snake board.
     * @return Returns the snake board.
     */
    @Override
    public SnakeBoard getSnakeBoard(){
        return snakeBoard;
    }
}
