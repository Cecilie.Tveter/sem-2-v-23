package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.Grid.CellPosition;

import java.util.Random;

public class Apple {
    private final Random random = new Random();
    private SnakeBoard board;
    public boolean applePlaced = false;
    public CellPosition appPos;


    /**
     * Inizialisering the board
     *
     * @param board
     */
    public Apple(SnakeBoard board) {
        this.board = board;
        newApple();

    }


    public CellPosition getAppPos() {
        return appPos;

    }

    /**
     * Placing a new apple random on the board.
     */
    public void newApple() {
        if (!applePlaced) {
            CellPosition updateApple = new CellPosition((random.nextInt(this.board.rows()) * 25 + 1),
                    random.nextInt(this.board.cols()) * 25 + 1);
            // Må endre på skaleringen av brettet hver gang
            // brettet blir dratt ut. Bugg.
            appPos = updateApple;
            applePlaced = true;
        }

    }
}

