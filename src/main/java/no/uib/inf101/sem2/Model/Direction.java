package no.uib.inf101.sem2.Model;

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
